using Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// GameScene 에서 사용되는 PlayerController입니다.
/// </summary>
public sealed class GamePlayerController : PlayerControllerBase
{
    /// <summary>
    /// 로컬 (자신의) 컨트롤러임을 나타냅니다.
    /// </summary>
    public bool isLocal {  get; private set; }

    /// <summary>
    /// 로컬 플레이어 컨트롤러가 생성되었을 때 호출됩니다.
    /// </summary>
    public void OnLocalPlayerControllerStarted()
    {
        isLocal = true;

        string nickname = NetworkManager.instance.nickname;

        
    }

    private void Start()
    {
        // 이 플레이어가 연결되었음을 서버에 알립니다.
        OnNewPlayerConnectedPacket onNewPlayerConnectedPacket = new(NetworkManager.instance.nickname);
        NetworkManager.instance.SendPacket(PacketType.OnThisPlayerConnected, onNewPlayerConnectedPacket);
    }

}
