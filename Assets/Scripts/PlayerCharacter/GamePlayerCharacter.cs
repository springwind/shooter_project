using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class GamePlayerCharacter : PlayerCharacterBase
{
    /// <summary>
    /// 닉네임 HUD 객체
    /// </summary>
    private NicknameHUD _NicknameHUD;


    /// <summary>
    /// 로컬 캐릭터임을 나타냅니다.
    /// </summary>
    public bool isLocal {  get; private set; }


    public override void OnControlStarted(PlayerControllerBase playerController)
    {
        base.OnControlStarted(playerController);

        // 로컬 캐릭터 설정
        isLocal = (playerController as GamePlayerController).isLocal;
    }

    public void SetNicknameHUD(NicknameHUD nicknameHUD)
    {
        _NicknameHUD = nicknameHUD;
    }
}
