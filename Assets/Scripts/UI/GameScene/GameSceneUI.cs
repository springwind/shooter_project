using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class GameSceneUI : MonoBehaviour
{
    private NicknamePanel _NicknamePanel;

    public NicknamePanel nicknamePanel =>
        _NicknamePanel ?? (_NicknamePanel = GetComponentInChildren<NicknamePanel>());
}
