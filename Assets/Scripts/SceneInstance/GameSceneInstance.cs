using Core;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEditor;
using UnityEditor.Search;
using UnityEngine;

public sealed class GameSceneInstance : SceneInstanceBase
{
    [Header("# 플레이어 캐릭터 Prefab")]
    [SerializeField]
    private GamePlayerCharacter _GamePlayerCharacterPrefab;

    [Header("# GameSceneUI")]
    public GameSceneUI m_GameSceneUI;

    private bool _IsOtherUserPacketReceuved;


    protected override void Awake()
    {
        base.Awake();

        NetworkManager.instance.AddPacketEvent(
            PacketType.OnNewPlayerConnected, OnNewPlayerConnected);

        NetworkManager.instance.AddPacketEvent(
            PacketType.GetConnectedPlayerList, OnConnectedPlayerListReceived);


        // 로컬에서 생성된 자신의 플레이어 컨트롤러를 얻습니다.
        GamePlayerController localPlayerController = playerController as GamePlayerController;

        // 로컬 캐릭터 생성
        GamePlayerCharacter localPlayerCharacter = CreateGamePlayerCharacter(
            NetworkManager.instance.nickname);

        // 로컬 캐릭터 조종 시작
        localPlayerController.StartControlCharacter(localPlayerCharacter);

        // 로컬 플레이어 컨트롤러 시작됨
        localPlayerController.OnLocalPlayerControllerStarted();
    }

    private void Start()
    {
        StartCoroutine(RequestOtherPlayerList());
    }

    private IEnumerator RequestOtherPlayerList()
    {
        while(true)
        {
            yield return new WaitForSeconds(2.0f);

            if (_IsOtherUserPacketReceuved)
            {
                Debug.Log("연결된 유저들 목록으로 받았으므로, 더이상 요청을 보내지 않습니다.");
                yield break;
            }
            else
            {
                Debug.Log("연결된 유저들 목록을 받지 못하였습니다.");

            }
            NetworkManager.instance.SendPacket(
                PacketType.GetConnectedPlayerList,
                new SimpleResponsePacket());
        }
    }


    private void OnDestroy()
    {
        NetworkManager.instance.RemovePacketEvent(
            PacketType.OnNewPlayerConnected, OnNewPlayerConnected);
        NetworkManager.instance.RemovePacketEvent(
            PacketType.GetConnectedPlayerList, OnConnectedPlayerListReceived);
    }

    private void OnNewPlayerConnected(PacketType packetType, object data)
    {
        OnNewPlayerConnectedPacket packet = (OnNewPlayerConnectedPacket)data;
        Debug.Log($"새로운 플레이어 연결됨! nickname : {packet.nickname}");

        // 다른 플레이어 캐릭터 생성
        Dispatcher.Enqueue(() => CreateOtherPlayerCharacter(packet.nickname));
    }

    private void OnConnectedPlayerListReceived(PacketType packetType, object data)
    {
        Debug.Log("연결된 유저들 목록 받음!");

        _IsOtherUserPacketReceuved = true;
        string localPlayerNickname = NetworkManager.instance.nickname;

        GetConnectedPlayerListPacket packet = (GetConnectedPlayerListPacket)data;

        string debug = "[NetworkMamager]기존유저 : ";
        foreach (string nick in packet.connectedPlayerNicknames)
            debug += $"{nick} / ";
        Debug.Log(debug);

        foreach (string nickname in packet.connectedPlayerNicknames)
        {
            if (nickname == localPlayerNickname) continue;

            Dispatcher.Enqueue(() => CreateOtherPlayerCharacter(nickname));
        }
    }

   

    /// <summary>
    /// 다른 플레이어의 캐릭터를 생성합니다.
    /// </summary>
    /// <param name="otherPlayerNickname"></param>
    private void CreateOtherPlayerCharacter(string otherPlayerNickname)
    {
        CreateGamePlayerCharacter(otherPlayerNickname);
    }

    /// <summary>
    /// GamePlayerCharacter 오브젝트를 생성합니다.
    /// </summary>
    /// <param name="nickname">유저의 닉네임을 전달합니다.</param>
    /// <returns>생성된 GamePlayerCharacter 오브젝트를 반환합니다.</returns>
    private GamePlayerCharacter CreateGamePlayerCharacter(string nickname)
    {
        GamePlayerCharacter newCharacter = Instantiate(_GamePlayerCharacterPrefab);

        // 오브젝트 이름 설정
        newCharacter.gameObject.name = $"{newCharacter.gameObject.name}_{nickname}";

        // 닉네임 HUD 생성
        NicknameHUD nicknameHUD = m_GameSceneUI.nicknamePanel.CreateNicknameHUD(nickname);
        newCharacter.SetNicknameHUD(nicknameHUD);

        return newCharacter;
    }
}
